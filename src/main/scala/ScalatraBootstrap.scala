import javax.servlet.ServletContext
import org.scalatra._
import com.qa.MainServlet

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    //File edited by

    // Mount servlets.
    context.mount(new MainServlet, "/*")


  }
}
